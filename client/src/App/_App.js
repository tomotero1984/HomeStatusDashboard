import './App.css';
import React, { Component, useState, useEffect } from 'react';
import Form from '../Form/Form.js';
import Table from '../Table/Table';
import Header from '../Header/Header';
import axios from 'axios';


class App extends Component {

  state = {
    data: null
  };

  fetchReq() {
    axios.get('http://localhost:5000/test', //proxy uri
    {
        headers: {
          authorization: ' xxxxxxxxxx' ,
          'Content-Type': 'application/json'
        } 
    }).then(function (response) {
        console.log(response);
    });
      // fetch('http://example.com/movies.json')
      // .then((response) => response.json())
      // .then((data) => console.log(data));
  }

  componentDidMount() {
    this.callBackendAPI()
      .then(res => this.setState({ data: res.express }))
      .catch(err => console.log(err));
  }
    // fetching the GET route from the Express server which matches the GET route from server.js
  callBackendAPI = async () => {
    const response = await fetch('/express_backend');
    const body = await response.json();

    if (response.status !== 200) {
      throw Error(body.message) 
    }
    return body;
  };


  render() {
    return (
      <div className="App">
        <Header />
        <Form />
        <button onClick={this.fetchReq}> fetchReq</button>
        <Table />
      </div>
    );
  }
}

export default App;
