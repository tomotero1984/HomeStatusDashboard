import React from 'react';
import Form from '../Form/Form';
import Table from '../Table/Table';
import Header from '../Header/Header';
import Person from '../Person/Person';
import Sensor from '../Sensor/Sensor';
import './App.css';

function App() {

  return (
    <div className='App'>
      <Header />
      <Sensor />
    </div>
  );
}
export default App;