import React from "react";
import "./Table.css";

class Table extends React.Component {
  // constructor(props) {
  //   super(props);
  // }

  render() {
    return (
      <table className="Table">
        <caption id="Title"> Test Table</caption>
        <thead>
          <tr>
            <th>Sensor</th>
            <th>Reading</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>test</td>
            <td>1234</td>
          </tr>
        </tbody>
      </table>
    );
  }
}

export default Table;
