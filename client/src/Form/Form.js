import React from "react";
import "./Form.css"

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {fname: '', lname: ''};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const fname = event.target.fname;
    const lname = event.target.lname;
    this.setState({
      ...this.state,
      [event.target.name]: event.target.value,
    });
  }

  handleSubmit(event) {
    alert('A name was submitted: ' + `${this.state.fname} ${this.state.lname}`);
    event.preventDefault();
  }
  // handleSubmit(event){
  //   fetch('http://example.com/movies.json')
  //   .then((response) => response.json())
  //   .then((data) => console.log(data));
  // }

  render() {  
  return (
      <div className="Form">
        <form onSubmit={this.handleSubmit}>
          <label>First name:<br/>
          <input type="text" id="fname" name="fname" value={this.state.fname} onChange={this.handleChange}/><br/>
          </label>
          <label>Last name:<br/>
          <input type="text" id="lname" name="lname" value={this.state.lname} onChange={this.handleChange}/><br/>
          </label><br/>
          <input type="submit" value="Submit"></input>
        </form>
      </div>
    );
  }
}
  
  export default Form;
