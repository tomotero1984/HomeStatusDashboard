import React, {useState, useEffect} from "react";

function Person (props){
  const [persons, setPersons] = useState(false);
  useEffect(() => {
    getPerson();
  }, []);
  function getPerson() {
    fetch('http://localhost:3001/persons')
      .then(response => {
        return response.text();
      })
      .then(data => {
        setPersons(data);
      });
  }

  function createPerson() {
    let name = prompt('Enter person name');
    let country = prompt('Enter person country');
    fetch('http://localhost:3001/persons', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({name, country}),
    })
      .then(response => {
        return response.text();
      })
      .then(data => {
        alert(data);
        getPerson();
      });
  }

  function deletePerson() {
    let id = prompt('Enter person id');
    fetch(`http://localhost:3001/persons/${id}`, {
      method: 'DELETE',
    })
      .then(response => {
        return response.text();
      })
      .then(data => {
        alert(data);
        getPerson();
      });
  }
  return (
      <div>
          {persons ? persons : 'There is no Persons data available'}
          <br />
          <button onClick={createPerson}>Add Person</button>
          <br />
          <button onClick={deletePerson}>Delete Person</button>
      </div>
  );
}

export default Person;
