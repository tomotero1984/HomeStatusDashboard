import React, { useState, useEffect } from "react";
import { w3cwebsocket as W3CWebSocket } from "websocket";

const client = new W3CWebSocket('ws://localhost:5001/');
client.onopen = () => {
  console.log('WebSocket Client Connected');
};
client.onmessage = (message) => {
  console.log(message);
};

function Sensor(props) {
  const [sensors, setSensors] = useState(false);
  useEffect(() => {
    getSensors();
  }, []);
  client.onmessage = (message) => {
    getSensors();
  };
  function getSensors() {
    fetch('http://localhost:3001/sensors')
      .then(response => {
        return response.text();
      })
      .then(data => {
        setSensors(data);
      });
  }

  return(
    <div>
      {sensors ?
      <table align="center">
        <caption id="Title"> Sensor Data</caption>
        <thead>
          <tr>
            <th>id</th>
            <th>Sensor</th>
            <th>Data</th>
          </tr>
        </thead>
        <tbody>
        {JSON.parse(sensors).map((val,key) => {
          return(
            <tr key={key}>
              <td>{val.id}</td>
              <td>{val.sensor}</td>
              <td>{val.data}</td>
            </tr>
          )
        })}
        </tbody>
      </table>
      : 'There is no sensors data available'}
    </div>
  );
}

export default Sensor;