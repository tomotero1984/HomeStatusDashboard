const Pool = require('pg').Pool
const pool = new Pool({
  user: 'tom',
  host: 'localhost',
  database: 'test',
  password: 'password',
  port: 5432,
});

const getPersons = () => {
  return new Promise(function(resolve, reject) {
    pool.query('SELECT * FROM person ORDER BY id ASC', (error, results) => {
      if (error) {
        reject(error)
      }
      resolve(results.rows)
    })
  }) 
}
const createPerson = (body) => {
  return new Promise(function(resolve, reject) {
    const { name, country } = body
    pool.query(`INSERT INTO person (id, name, country) VALUES (nextval('person_id_seq'), $1, $2) RETURNING *`, [name, country],
      (error, results) => {
        if (error) {
          reject(error)
        }
        resolve(`A new person has been added added: ${results.rows[0]}`)
    })
  })
}
const deletePerson = (id) => {
  return new Promise(function(resolve, reject) {
    pool.query(`DELETE FROM person WHERE id = ${id}`,
    (error, results) => {
      if (error) {
        reject(error)
      }
      resolve(`Person deleted with ID: ${id}`)
    })
  })
}
  
module.exports = {
  getPersons,
  createPerson,
  deletePerson,
}