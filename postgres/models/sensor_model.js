const Pool = require('pg').Pool
const createSubscriber = require("pg-listen")

pg_config = {
  user: 'tom',
  host: 'localhost',
  database: 'homestatusdashboard',
  password: 'password',
  port: 5432,
}
const pool = new Pool(pg_config);
const subscriber = createSubscriber(pg_config)

subscriber.notifications.on("sensors_changed", (payload) => {
  console.log("Received notification in 'sensors_changed':", payload)
})

subscriber.events.on("error", (error) => {
  console.error("Fatal database connection error:", error)
  process.exit(1)
})

process.on("exit", () => {
  subscriber.close()
})


const getSensors = () => {
  return new Promise(function(resolve, reject) {
    pool.query(
      `SELECT * FROM public."sensors" ORDER BY id ASC`, 
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(results.rows);
      }
    );
  });
}

const createSensor = (body) => {
  return new Promise(function(resolve, reject) {
    const { sensor, data } = body;
    pool.query(
      `INSERT INTO sensors (sensor, data) VALUES ('${sensor}', ${data})`,
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(`A new sensor has been added added: ${results}`);
      }
    );
  });
}

const deleteSensor = (id) => {
  return new Promise(function(resolve, reject) {
    pool.query(
      `DELETE FROM sensors WHERE id = ${id}`,
      (error, results) => {
        if (error) {
          reject(error);
        }
        resolve(`Sensor deleted with ID: ${id}`)
      }
    )
  })
}

async function connect() {
  await subscriber.connect()
  await subscriber.listenTo("sensors_changed")
}

module.exports = {
  getSensors,
  createSensor,
  deleteSensor,
  connect,
  subscriber
}