const express = require('express')
const cors = require("cors")
const sensor_model = require('./models/sensor_model')

const app = express()
const port = 3001
app.use(cors())
app.use(express.json())


// Sensors
sensor_model.connect();
app.get('/sensors', (req, res) =>{
  sensor_model.getSensors()
  .then(response => {
    res.status(200).send(response);
  })
  .catch(error => {
    res.status(500).send(error);
  });
});

app.post('/sensors', (req, res) =>{
  sensor_model.createSensor(req.body)
  .then(response => {
    res.status(200).send(response);
  })
  .catch(error => {
    res.status(500).send(error);
  });
});

app.delete('/sensors/:id', (req, res) => {
  sensor_model.deleteSensor(req.params.id)
  .then(response => {
    res.status(200).send(response);
  })
  .catch(error => {
    res.status(500).send(error);
  });
});

app.listen(port, () => {
  console.log(`App running on port ${port}.`)
})


//Websocket
const http = require('http');
const { client } = require('websocket')
const server = http.createServer();
const ws_port = 5001;
server.listen(ws_port);
const webSocketServer = require('websocket').server;
console.log(`WebSocket running on port ${ws_port}`)
const wsServer = new webSocketServer({
  httpServer: server,
  keepalive: true,
  keepaliveInterval: 5000
});
const clients = {};
const getUniqueID = () => {
  const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  return s4() + s4() + '-' + s4();
};

wsServer.on('request', function(request) {
  var userID = getUniqueID();
  console.log((new Date()) + ' Recieved a new connection from origin ' + request.origin + '.');
  // You can rewrite this part of the code to accept only the requests from allowed origin
  const connection = request.accept(null, request.origin);
  clients[userID] = connection;
  console.log('connected: ' + userID + ' in ' + Object.getOwnPropertyNames(clients))
  connection.on("message", (message) => {
    console.log(message.utf8Data);
  })
});
sensor_model.subscriber.notifications.on("sensors_changed", () => {
  wsServer.broadcast("sensors_changed")
})



/*TODO
  1. add a "who is this" method
  2. recieve the messages from the sensor
  3. send the sensor messages to the database
  4. (optional) send the messages to the website for graphing
*/