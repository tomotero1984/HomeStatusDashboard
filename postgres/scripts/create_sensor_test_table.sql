CREATE TABLE IF NOT EXISTS sensors (
    id SERIAL PRIMARY KEY UNIQUE NOT NULL,
    sensor TEXT NOT NULL,
    data INTEGER NOT NULL
)